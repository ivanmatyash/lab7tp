//
//  ViewController.h
//  lab72
//
//  Created by Иван Матяш on 4/29/16.
//  Copyright (c) 2016 Иван Матяш. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>

@interface ViewController : GLKViewController

@end

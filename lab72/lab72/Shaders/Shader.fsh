//
//  Shader.fsh
//  lab72
//
//  Created by Иван Матяш on 4/29/16.
//  Copyright (c) 2016 Иван Матяш. All rights reserved.
//

varying lowp vec4 colorVarying;

void main()
{
    gl_FragColor = colorVarying;
}

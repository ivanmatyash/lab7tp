//
//  main.m
//  lab72
//
//  Created by Иван Матяш on 4/29/16.
//  Copyright (c) 2016 Иван Матяш. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

//
//  AppDelegate.h
//  lab7_1
//
//  Created by Иван Матяш on 4/28/16.
//  Copyright (c) 2016 Иван Матяш. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
